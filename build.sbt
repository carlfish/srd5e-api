name := """srdapi"""

version := "0.4"


scalaVersion := "2.11.7"

// Set of "good" compile options from https://tpolecat.github.io/2014/04/11/scalac-flags.html
scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Ywarn-unused-import",
  "-Yno-predef"
)

scalacOptions in (Compile, console) ~= (_ filterNot (_ == "-Ywarn-unused-import"))
scalacOptions in (Test, console) := (scalacOptions in (Compile, console)).value

initialCommands in console := """
  import scala.Predef._
  import scalaz._
  import srdapi._
  |""".stripMargin

// Change this to another test framework if you prefer
libraryDependencies ++= Seq(
  "io.argonaut" %% "argonaut" % "6.1",
  "org.scalaz" %% "scalaz-core" % "7.1.4",
  "org.scalaz" %% "scalaz-effect" % "7.1.4",
  "org.scalaz" %% "scalaz-concurrent" % "7.1.4",

  // HTTP Server and underlying impl
  "org.http4s" %% "http4s-dsl"          % "0.12.3",
  "org.http4s" %% "http4s-argonaut"          % "0.12.3",
  "org.http4s" %% "http4s-blaze-server" % "0.12.3",

  // HTTP client and related deps
  "org.slf4j" % "slf4j-simple" % "1.7.12",

  // Test deps
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.scalacheck" %% "scalacheck" % "1.12.4" % "test"
)

mainClass := Some("srdapi.spells.SpellsWebApp")

dockerRepository := Some("us.gcr.io/carl-fish-services")

enablePlugins(JavaAppPackaging)

enablePlugins(DockerPlugin)

dockerExposedPorts := Seq(8080)

fork in run := true
