# SRD 5e API #

Currently, the only resource supported is a spell lookup for Slack integration. Check the wiki for how to hook it up.

Game content in this repository is Copyright Wizards of the Coast, made available under the Open Gaming License Version 1.0a.

For all other licensing, check LICENSE.txt
