package srdapi.spells

import Predef._
import scalaz._
import scalaz.concurrent.Task
import org.http4s._
import org.http4s.dsl._
import org.http4s.argonaut._
import org.http4s.server.blaze.BlazeBuilder
import _root_.argonaut._, Argonaut._

object SpellsWebApp extends App {
  BlazeBuilder.bindHttp(8080, "0.0.0.0")
    .mountService(SpellService.service)
    .run
    .awaitShutdown()
}

object SpellService {
  val spellList: List[Spell] =
    io.Source.fromInputStream(getClass.getResourceAsStream("spells.json"))
      .mkString
      .decodeOption[List[Spell]].getOrElse { throw new IllegalStateException("Could not load spell list")}

  case class Spell(
      name: String,
      typeLine: String,
      classes: Seq[String],
      castingTime: String,
      range: String,
      components: String,
      duration: String,
      description: String,
      higherLevels: Option[String]
  ) {
    lazy val lookupName = toLookupName(name)
  }

  def toLookupName(s: String) = s.toLowerCase.filter(Character.isLetterOrDigit)

  implicit def SpellDecodeJson = jdecode9L(Spell.apply)(
    "name", "type-line", "classes", "casting-time", "range", "components",
     "duration", "description", "at-higher-levels")

  def toSlackResponse(s: Spell): Json = {
    ("response_type" := "in_channel") ->:
    ("attachments" := List(
      ("title" := s.name) ->:
      ("text" := ("_" + s.typeLine + "_")) ->:
      ("mrkdwn_in" := List("text", "fields")) ->:
      ("fields" := makeFields(s)) ->:
      jEmptyObject
    )) ->:
    jEmptyObject
  }

  def toSlackError(msg: String): Json = {
    ("text" := msg) ->:
    jEmptyObject
  }

  def spellNotFoundError(spellName: String, suggestions: List[String]) = {
    "No spell called " + spellName + " found." + (if (suggestions.isEmpty) {
      ""
    } else {
      " Possible matches: " + suggestions.mkString(", ") + "."
    })
  }

  def makeFields(s: Spell): List[Json] = {
    List(
      makeField("Casting Time", s.castingTime, true),
      makeField("Range", s.range, true),
      makeField("Components", s.components, true),
      makeField("Duration", s.duration, true),
      makeField("Effect", s.description, false)) ++
      s.higherLevels.map(makeField("At Higher Levels", _, false)).toList
  }

  def makeField(title: String, value: String, short: Boolean): Json = {
    ("title" := title) ->:
    ("value" := value) ->:
    (if (short) Some("short" := true) else None) ->?:
    jEmptyObject
  }

  def findSpell(n: String): \/[List[String], Spell] = {
    val nn = toLookupName(n)

    spellList.find(_.lookupName == nn).map(\/-(_)).getOrElse(-\/(findClosest(nn).map(_._2.name)))
  }

  def findClosest(lookupName: String): List[(Double, Spell)] = {
    def doInsert(score: Double, spell: Spell, spells: List[(Double, Spell)]): List[(Double, Spell)] = spells match {
      case Nil => List((score, spell))
      case (sc, sp) :: ss if (sc <= score) => (sc, sp) :: doInsert(score, spell, ss)
      case (sc, sp) :: ss => (score, spell) :: spells
    }

    def insert(score: Double, spell: Spell, spells: List[(Double, Spell)]): List[(Double, Spell)] = {
      val shouldTruncate = spells.length >= 5
      val pass = (shouldTruncate && spells.head._1 < score) || score < 0.8

      val ret = if (pass) spells else doInsert(score, spell, spells)
      if (shouldTruncate) ret.drop(1) else ret
    }

    def findBestMatches(spells: List[Spell], matches: List[(Double, Spell)]): List[(Double, Spell)] = spells match {
      case Nil => matches
      case s :: ss => {
        val score = org.pastiche.imported.JaroWinkler.similarity(lookupName, s.lookupName)

        findBestMatches(ss, insert(score, s, matches))
      }
    }

    findBestMatches(spellList, List()).reverse
  }

  def slackQuery(req: Request) = {
    req.decode[UrlForm] { data =>
      val spellName = data.getFirstOrElse("text", "").trim

      if (spellName.isEmpty) Ok(toSlackError("I need the name of a spell to look up."))
      else findSpell(spellName)
        .bimap(e => Ok(toSlackError(spellNotFoundError(spellName, e))), s => Ok(toSlackResponse(s))).merge
    }
  }

  val service = HttpService {
    case req @ GET -> Root => Ok("Running")
    case req @ POST -> Root / "srd" / "slack" / "spells" => slackQuery(req)
  }
}
