package srdapi.tools

import Predef._
import scalaz.stream._
import _root_.argonaut._, Argonaut._


import java.io._

case class Spell(
    name: String,
    typeLine: String,
    classes: Seq[String],
    castingTime: String,
    range: String,
    components: String,
    duration: String,
    description: String,
    higherLevels: Option[String]
)

/**
 * Parses the particular format of spells I got from
 * https://github.com/gguillotte/mdsrd5
 */
object MdSpellsParser {
  implicit def SpellEncodeJson = EncodeJson((s: Spell) =>
    ("name" := s.name) ->:
    ("type-line" := s.typeLine) ->:
    ("classes" := s.classes) ->:
    ("casting-time" := s.castingTime) ->:
    ("range" := s.range) ->:
    ("components" := s.components) ->:
    ("duration" := s.duration) ->:
    ("description" := s.description) ->:
    s.higherLevels.map("at-higher-levels" := _) ->?:
    jEmptyObject
  )

  def convert(in: File, out: File): Unit = {
    io.linesR(new FileInputStream(in))
      .pipe(splitBySpells)
      .map(parseSpell)
      .map(_.asJson)
      .map(_.spaces4)
      .intersperse(",\n")
      .pipe(bookend("[\n", "\n]"))
      .pipe(text.utf8Encode)
      .to(io.fileChunkW(out.toString)).run.run
  }

  def bookend(before: String, after: String): Process1[String, String] = Process.receive1[String, String] { s =>
    def go: Process1[String, String] = process1.lift[String, String](x => x) ++ go
    (Process.emit(before) ++ Process.emit(s) ++ go) onComplete (Process.emit(after))
  }

  def splitBySpells: Process1[String, Vector[String]] = {
    def go(lines: Vector[String]): Process1[String, Vector[String]] =
      Process.receive1Or[String, Vector[String]] {
          if (!lines.isEmpty)
           Process.emit(lines)
         else
           Process.empty
       } { line =>
        if (lines.isEmpty) {
          if (line.startsWith("#### "))
            go(Vector(line))
          else
            go(lines)
        } else if (line.startsWith("#### ")) {
          Process.emit(lines) ++ go(Vector(line))
        } else {
          go(lines :+ line)
        }
      }

    go(Vector())
  }

  def parseSpell: Vector[String] => Spell = {
    import scala.util.matching.Regex

    def name(s: String) = s.substring(5)
    def typeLine(s: String) = s.substring(1, s.length - 1)
    def classes(s: String) = Vector() ++ new Regex("(?<=\\[).*?(?=\\])").findAllIn(s)
    def castingTime(s: String) = s.substring(18)
    def range(s: String) = s.substring(11)
    def components(s: String) = s substring(16)
    def duration(s: String) = s.substring(14)
    def rest(v: Vector[String]) = v.span(!_.startsWith("*At Higher Levels")) match {
      case (desc, rest) => {
        val prest = if (rest.isEmpty) {
          None
        } else {
          Some(rest.updated(0, rest(0).substring(20)).mkString("\n").trim)
        }

        (desc.mkString("\n").trim, prest)
      }
    }
    def makeSpell(lines: Vector[String]) = rest(lines.drop(14)) match {
      case (desc, higherLevels) => {
        Spell(
          name(lines(0)),
          typeLine(lines(2)),
          classes(lines(4)),
          castingTime(lines(6)),
          range(lines(8)),
          components(lines(10)),
          duration(lines(12)),
          desc,
          higherLevels
        )
      }
    }

    makeSpell _
  }

  def toJson: Process1[Spell, Json] = process1.lift[Spell, Json](_.asJson)

  def toJsonString: Process1[Json, String] = process1.lift[Json, String](_.spaces4)

  def collateList: Process1[Json, Json] =
    (process1.fold[Json, Vector[Json]](Vector[Json]())(_ :+ _))
      .map(jArrayElements(_:_*))

}
